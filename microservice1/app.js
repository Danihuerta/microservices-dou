const express = require('express')
const app = express()
const PORT = 3001

app.use(express.json())
app.get('/microservice1', (req, res, next) => {
    res.send('Hello From Microservice1')
})

app.listen(PORT, () => {
    console.log('Microservice1 listen on port ' + PORT)
})
