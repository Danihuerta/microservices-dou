const express = require('express')
const app = express()
const PORT = 3002

app.use(express.json())
app.get('/microservice2', (req, res, next) => {
    res.send('Hello From Microservice2')
})

app.listen(PORT, () => {
    console.log('Microservice2 listen on port ' + PORT)
})
