# Microservices DOU

Repository of the activity of DigitalOnUs university about microservices

## Instructions to run this project
We are going to run a microservices infrastructure using two simple microservices and one API Gateway.
So first of all we need to run the microservices using the compose-file:

### Run microservices
Executing the next command will run in two different containers the service1 and service2.

    docker-compose up

The images are already pushed in my Docker Hub, for more information please visit: 
- https://hub.docker.com/repository/docker/971213/microservice1
- https://hub.docker.com/repository/docker/971213/microservice2

### Run the API Gateway
We want to communicate to the microservices using the API Gateway, that's we need to run it. It is going to redirect the user http request (using the API endpoints) to the respective Microservice's endpoints.

Placed in the *apigateway* folder execute:

    npm install

All the dependencies will be installed and the app will be running in *http://localhost:3000*

### Test the infrastructure
Now we are able to reach each microservice using the API Gateway.
To test the microservice 1 we need to type in the browser:

    http://localhost:3000/microservice1

And we have to receive a response with the content of *Hello From Microservice1*

We can to the same with microservice 2 modifying the url:

   http://localhost:3000/microservice2

Getting a response with *Hello From Microservice2*

