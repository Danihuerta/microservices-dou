const express = require('express')
const router = express.Router()
const axios = require('axios')

router.get('/microservice1', (req,res)=>{
    axios.get('http://localhost:4001/microservice1').then((response) => {
        res.send(response.data)
    })
})

router.get('/microservice2', (req,res)=>{
    axios.get('http://localhost:4002/microservice2').then((response) => {
        res.send(response.data)
    })
})


module.exports = router